﻿using InstallerTest.Windows.Base;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;

namespace InstallerTest.Windows.Readme
{
    internal class ReadmeWindow : BaseWindow, IReadmeWindow
    {
        public WinEdit ReadmeText
        {
            get
            {
                if (this.readmeText == null)
                {
                    this.readmeText = new WinEdit(this);

                    this.readmeText.TechnologyName = "MSAA";
                    this.readmeText.SearchProperties[UITestControl.PropertyNames.ControlType] = "Edit";
                }
                return this.readmeText;
            }
        }

        private WinEdit readmeText;
    }
}
