﻿using InstallerTest.Windows.Base;
using Microsoft.VisualStudio.TestTools.UITesting;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;

namespace InstallerTest.Windows.SelectFeatures
{
    internal class SelectFeaturesWindow : BaseWindow, ISelectFeaturesWindow
    {
        public WinCheckBox ApxCpsCheckbox
        {
            get
            {
                if (this.apxCpsCheckbox == null)
                {
                    this.apxCpsCheckbox = new WinCheckBox(this);

                    this.apxCpsCheckbox.TechnologyName = "MSAA";
                    this.apxCpsCheckbox.SearchProperties.Add(new PropertyExpression(UITestControl.PropertyNames.Name, "APX CPS (Version", PropertyExpressionOperator.Contains));
                    this.apxCpsCheckbox.SearchProperties[UITestControl.PropertyNames.ControlType] = "CheckBoxTreeItem";
                }
                return this.apxCpsCheckbox;
            }
        }

        public WinCheckBox FrenchCheckbox
        {
            get
            {
                if (this.frenchCheckbox == null)
                {
                    this.frenchCheckbox = new WinCheckBox(this);

                    this.frenchCheckbox.TechnologyName = "MSAA";
                    this.frenchCheckbox.SearchProperties[UITestControl.PropertyNames.Name] = "French (Canadian)";
                    this.frenchCheckbox.SearchProperties[UITestControl.PropertyNames.ControlType] = "CheckBoxTreeItem";
                }
                return this.frenchCheckbox;
            }
        }

        public WinCheckBox SpanishCheckbox
        {
            get
            {
                if (this.spanishCheckbox == null)
                {
                    this.spanishCheckbox = new WinCheckBox(this);

                    this.spanishCheckbox.TechnologyName = "MSAA";
                    this.spanishCheckbox.SearchProperties[UITestControl.PropertyNames.Name] = "Spanish";
                    this.spanishCheckbox.SearchProperties[UITestControl.PropertyNames.ControlType] = "CheckBoxTreeItem";
                }
                return this.spanishCheckbox;
            }
        }

        public WinCheckBox PortugueseCheckbox
        {
            get
            {
                if (this.portugueseCheckbox == null)
                {
                    this.portugueseCheckbox = new WinCheckBox(this);

                    this.portugueseCheckbox.TechnologyName = "MSAA";
                    this.portugueseCheckbox.SearchProperties[UITestControl.PropertyNames.Name] = "Portuguese";
                    this.portugueseCheckbox.SearchProperties[UITestControl.PropertyNames.ControlType] = "CheckBoxTreeItem";
                }
                return this.portugueseCheckbox;
            }
        }

        public WinCheckBox HebrewCheckbox
        {
            get
            {
                if (this.hebrewCheckbox == null)
                {
                    this.hebrewCheckbox = new WinCheckBox(this);

                    this.hebrewCheckbox.TechnologyName = "MSAA";
                    this.hebrewCheckbox.SearchProperties[UITestControl.PropertyNames.Name] = "Hebrew";
                    this.hebrewCheckbox.SearchProperties[UITestControl.PropertyNames.ControlType] = "CheckBoxTreeItem";
                }
                return this.hebrewCheckbox;
            }
        }

        public WinCheckBox RussianCheckbox
        {
            get
            {
                if (this.russianCheckbox == null)
                {
                    this.russianCheckbox = new WinCheckBox(this);

                    this.russianCheckbox.TechnologyName = "MSAA";
                    this.russianCheckbox.SearchProperties[UITestControl.PropertyNames.Name] = "Russian";
                    this.russianCheckbox.SearchProperties[UITestControl.PropertyNames.ControlType] = "CheckBoxTreeItem";
                }
                return this.russianCheckbox;
            }
        }

        public WinCheckBox ChineseCheckbox
        {
            get
            {
                if (this.chineseCheckbox == null)
                {
                    this.chineseCheckbox = new WinCheckBox(this);

                    this.chineseCheckbox.TechnologyName = "MSAA";
                    this.chineseCheckbox.SearchProperties[UITestControl.PropertyNames.Name] = "Chinese (Traditional)";
                    this.chineseCheckbox.SearchProperties[UITestControl.PropertyNames.ControlType] = "CheckBoxTreeItem";
                }
                return this.chineseCheckbox;
            }
        }

        public WinCheckBox ArabicCheckbox
        {
            get
            {
                if (this.arabicCheckbox == null)
                {
                    this.arabicCheckbox = new WinCheckBox(this);

                    this.arabicCheckbox.TechnologyName = "MSAA";
                    this.arabicCheckbox.SearchProperties[UITestControl.PropertyNames.Name] = "Arabic";
                    this.arabicCheckbox.SearchProperties[UITestControl.PropertyNames.ControlType] = "CheckBoxTreeItem";
                }
                return this.arabicCheckbox;
            }
        }

        public WinCheckBox RadioManagementClientCheckbox
        {
            get
            {
                if (this.radioManagementClientCheckbox == null)
                {
                    this.radioManagementClientCheckbox = new WinCheckBox(this);

                    this.radioManagementClientCheckbox.TechnologyName = "MSAA";
                    this.radioManagementClientCheckbox.SearchProperties.Add(new PropertyExpression(UITestControl.PropertyNames.Name, "Radio Management Client (Version", PropertyExpressionOperator.Contains));
                    this.radioManagementClientCheckbox.SearchProperties[UITestControl.PropertyNames.ControlType] = "CheckBoxTreeItem";
                }
                return this.radioManagementClientCheckbox;
            }
        }

        public WinCheckBox RadioManagementServerCheckbox
        {
            get
            {
                if (this.radioManagementServerCheckbox == null)
                {
                    this.radioManagementServerCheckbox = new WinCheckBox(this);

                    this.radioManagementServerCheckbox.TechnologyName = "MSAA";
                    this.radioManagementServerCheckbox.SearchProperties.Add(new PropertyExpression(UITestControl.PropertyNames.Name, "Radio Management Server (Version", PropertyExpressionOperator.Contains));
                    this.radioManagementServerCheckbox.SearchProperties[UITestControl.PropertyNames.ControlType] = "CheckBoxTreeItem";
                }
                return this.radioManagementServerCheckbox;
            }
        }

        public WinCheckBox AutoUpdateEnableCheckbox
        {
            get
            {
                if (this.autoUpdateEnableChekcbox == null)
                {
                    this.autoUpdateEnableChekcbox = new WinCheckBox(this);

                    this.autoUpdateEnableChekcbox.TechnologyName = "MSAA";
                    this.autoUpdateEnableChekcbox.SearchProperties[UITestControl.PropertyNames.Name] = "AutoUpdate Enable";
                    this.autoUpdateEnableChekcbox.SearchProperties[UITestControl.PropertyNames.ControlType] = "CheckBoxTreeItem";
                }
                return this.autoUpdateEnableChekcbox;
            }
        }

        public WinCheckBox RadioManagementDeviceProgrammerCheckbox
        {
            get
            {
                if (this.radioManagementDeviceProgrammerCheckbox == null)
                {
                    this.radioManagementDeviceProgrammerCheckbox = new WinCheckBox(this);

                    this.radioManagementDeviceProgrammerCheckbox.TechnologyName = "MSAA";
                    this.radioManagementDeviceProgrammerCheckbox.SearchProperties.Add(new PropertyExpression(UITestControl.PropertyNames.Name, "Radio Management Device Programmer (Version", PropertyExpressionOperator.Contains));
                    this.radioManagementDeviceProgrammerCheckbox.SearchProperties[UITestControl.PropertyNames.ControlType] = "CheckBoxTreeItem";
                }
                return this.radioManagementDeviceProgrammerCheckbox;
            }
        }

        public WinCheckBox RadioManagementJobProcessorCheckbox
        {
            get
            {
                if (this.radioManagementJobProcessor == null)
                {
                    this.radioManagementJobProcessor = new WinCheckBox(this);

                    this.radioManagementJobProcessor.TechnologyName = "MSAA";
                    this.radioManagementJobProcessor.SearchProperties.Add(new PropertyExpression(UITestControl.PropertyNames.Name, "Radio Management Job Processor (Version", PropertyExpressionOperator.Contains));
                    this.radioManagementJobProcessor.SearchProperties[UITestControl.PropertyNames.ControlType] = "CheckBoxTreeItem";
                }
                return this.radioManagementJobProcessor;
            }
        }

        public WinCheckBox ApxFamilyTunerCheckbox
        {
            get
            {
                if (this.apxFamilyTunerCheckbox == null)
                {
                    this.apxFamilyTunerCheckbox = new WinCheckBox(this);

                    this.apxFamilyTunerCheckbox.TechnologyName = "MSAA";
                    this.apxFamilyTunerCheckbox.SearchProperties.Add(new PropertyExpression(UITestControl.PropertyNames.Name, "ApxFamilyTuner (Version", PropertyExpressionOperator.Contains));
                    this.apxFamilyTunerCheckbox.SearchProperties[UITestControl.PropertyNames.ControlType] = "CheckBoxTreeItem";
                }
                return this.apxFamilyTunerCheckbox;
            }
        }

        public WinCheckBox AdvancedKeysAdministratorCheckbox
        {
            get
            {
                if (this.advancedKeysAdministratorCheckbox == null)
                {
                    this.advancedKeysAdministratorCheckbox = new WinCheckBox(this);

                    this.advancedKeysAdministratorCheckbox.TechnologyName = "MSAA";
                    this.advancedKeysAdministratorCheckbox.SearchProperties.Add(new PropertyExpression(UITestControl.PropertyNames.Name, "Advanced Keys Administrator (Version", PropertyExpressionOperator.Contains));
                    this.advancedKeysAdministratorCheckbox.SearchProperties[UITestControl.PropertyNames.ControlType] = "CheckBoxTreeItem";
                }
                return this.advancedKeysAdministratorCheckbox;
            }
        }

        public WinCheckBox ArsDataAdministratorCheckbox
        {
            get
            {
                if (this.arsDataAdministratorCheckbox == null)
                {
                    this.arsDataAdministratorCheckbox = new WinCheckBox(this);

                    this.arsDataAdministratorCheckbox.TechnologyName = "MSAA";
                    this.arsDataAdministratorCheckbox.SearchProperties.Add(new PropertyExpression(UITestControl.PropertyNames.Name, "ARS Data Administrator (Version", PropertyExpressionOperator.Contains));
                    this.arsDataAdministratorCheckbox.SearchProperties[UITestControl.PropertyNames.ControlType] = "CheckBoxTreeItem";
                }
                return this.arsDataAdministratorCheckbox;
            }
        }

        public WinCheckBox ApxMigrationAssistantCheckbox
        {
            get
            {
                if (this.apxMigrationAssistantCheckbox == null)
                {
                    this.apxMigrationAssistantCheckbox = new WinCheckBox(this);

                    this.apxMigrationAssistantCheckbox.TechnologyName = "MSAA";
                    this.apxMigrationAssistantCheckbox.SearchProperties.Add(new PropertyExpression(UITestControl.PropertyNames.Name, "APX Migration Assistant (Version", PropertyExpressionOperator.Contains));
                    this.apxMigrationAssistantCheckbox.SearchProperties[UITestControl.PropertyNames.ControlType] = "CheckBoxTreeItem";
                }
                return this.apxMigrationAssistantCheckbox;
            }
        }

        public IDictionary<Features, WinCheckBox> FeaturesListDictionary => new Dictionary<Features, WinCheckBox>
        {
            { Features.ApxCps, ApxCpsCheckbox },
            { Features.French, FrenchCheckbox },
            { Features.Spanish, SpanishCheckbox },
            { Features.Portuguese, PortugueseCheckbox },
            { Features.Hebrew, HebrewCheckbox},
            { Features.Russian, RussianCheckbox },
            { Features.Chinese, ChineseCheckbox },
            { Features.Arabic, ArabicCheckbox },
            { Features.RadioManagementClient, RadioManagementClientCheckbox },
            { Features.RadioManagementServer, RadioManagementServerCheckbox },
            { Features.AutoUpdateEnable, AutoUpdateEnableCheckbox },
            { Features.RadioManagementDeviceProgrammer, RadioManagementDeviceProgrammerCheckbox },
            { Features.RadioManagementJobProcessor, RadioManagementJobProcessorCheckbox},
            { Features.ApxFamilyTuner, ApxFamilyTunerCheckbox },
            { Features.AdvancedKeysAdministrator, AdvancedKeysAdministratorCheckbox },
            { Features.ArsDataAdministrator, ArsDataAdministratorCheckbox },
            { Features.ApxMigrationAssistant, ApxMigrationAssistantCheckbox }
        };

        public IDictionary<Features, WinCheckBox> ComponentsListDictionary => new Dictionary<Features, WinCheckBox>
        {
            {Features.ApxCps, ApxCpsCheckbox},
            {Features.RadioManagementClient, RadioManagementClientCheckbox},
            {Features.RadioManagementServer, RadioManagementServerCheckbox},
            {Features.RadioManagementDeviceProgrammer, RadioManagementDeviceProgrammerCheckbox},
            {Features.RadioManagementJobProcessor, RadioManagementJobProcessorCheckbox},
            {Features.ApxFamilyTuner, ApxFamilyTunerCheckbox},
            {Features.AdvancedKeysAdministrator, AdvancedKeysAdministratorCheckbox},
            {Features.ArsDataAdministrator, ArsDataAdministratorCheckbox},
            {Features.ApxMigrationAssistant, ApxMigrationAssistantCheckbox}
        };

        private WinCheckBox apxCpsCheckbox;
        private WinCheckBox frenchCheckbox;
        private WinCheckBox spanishCheckbox;
        private WinCheckBox portugueseCheckbox;
        private WinCheckBox hebrewCheckbox;
        private WinCheckBox russianCheckbox;
        private WinCheckBox chineseCheckbox;
        private WinCheckBox arabicCheckbox;
        private WinCheckBox radioManagementClientCheckbox;
        private WinCheckBox radioManagementServerCheckbox;
        private WinCheckBox autoUpdateEnableChekcbox;
        private WinCheckBox radioManagementDeviceProgrammerCheckbox;
        private WinCheckBox radioManagementJobProcessor;
        private WinCheckBox apxFamilyTunerCheckbox;
        private WinCheckBox advancedKeysAdministratorCheckbox;
        private WinCheckBox arsDataAdministratorCheckbox;
        private WinCheckBox apxMigrationAssistantCheckbox;
    }
}
