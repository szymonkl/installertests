﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;

namespace InstallerTest.Windows.SelectFeatures
{
    internal interface ISelectFeaturesWindow
    {
        /// <summary>
        /// Dictionary which contains Feature options and matching checkbox
        /// </summary>
        IDictionary<Features, WinCheckBox> FeaturesListDictionary { get; }
    }
}