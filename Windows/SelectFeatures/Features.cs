﻿namespace InstallerTest.Windows.SelectFeatures
{
    public enum Features
    {
        ApxCps,
        French,
        Spanish,
        Portuguese,
        Hebrew, 
        Russian,
        Chinese,
        Arabic,
        RadioManagementClient,
        RadioManagementServer,
        AutoUpdateEnable,
        RadioManagementDeviceProgrammer,
        RadioManagementJobProcessor,
        ApxFamilyTuner,
        AdvancedKeysAdministrator,
        ArsDataAdministrator,
        ApxMigrationAssistant
    }
}
