﻿using System.Collections.Generic;
using InstallerTest.Windows.Base;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;

namespace InstallerTest.Windows.AddRadios
{
    internal class AddRadiosWindow : BaseWindow, IAddRadiosWindow
    {
        public WinRadioButton DoNotAddRadiosRadioButton
        {
            get
            {
                if (this.doNotAddRadiosRadioButton == null)
                {
                    this.doNotAddRadiosRadioButton = new WinRadioButton(this);

                    this.doNotAddRadiosRadioButton.TechnologyName = "MSAA";
                    this.doNotAddRadiosRadioButton.SearchProperties.Add(
                        UITestControl.PropertyNames.Name, "Do not automatically add new radio to the RM database");
                    this.doNotAddRadiosRadioButton.SearchProperties[UITestControl.PropertyNames.ControlType] = "RadioButton";
                }
                return this.doNotAddRadiosRadioButton;
            }
        }

        public WinRadioButton CreateTemplateRadioButton
        {
            get
            {
                if (this.createTemplateRadioButton == null)
                {
                    this.createTemplateRadioButton = new WinRadioButton(this);
                    this.createTemplateRadioButton.TechnologyName = "MSAA";
                    this.createTemplateRadioButton.SearchProperties.Add(
                        UITestControl.PropertyNames.Name, "Automatically read new radios and create a template");
                    this.createTemplateRadioButton.SearchProperties[UITestControl.PropertyNames.ControlType] = "RadioButton";
                }
                return this.createTemplateRadioButton;
            }
        }

        public WinRadioButton CreateConfigurationRadioButton
        {
            get
            {
                if (this.createConfigurationRadioButton == null)
                {
                    this.createConfigurationRadioButton = new WinRadioButton(this);
                    this.createConfigurationRadioButton.TechnologyName = "MSAA";
                    this.createConfigurationRadioButton.SearchProperties.Add(
                        UITestControl.PropertyNames.Name, "Automatically read new radios and create a configuration");
                    this.createConfigurationRadioButton.SearchProperties[UITestControl.PropertyNames.ControlType] = "RadioButton";
                }
                return this.createConfigurationRadioButton;
            }
        }

        public IDictionary<AddRadios, WinRadioButton> AddRadiosOptions => new Dictionary<AddRadios, WinRadioButton>
        {
            {AddRadios.DoNotAddRadios, DoNotAddRadiosRadioButton},
            {AddRadios.CreateConfiguration, CreateConfigurationRadioButton},
            {AddRadios.CreateTemplate, CreateTemplateRadioButton}
        };

        private WinRadioButton doNotAddRadiosRadioButton;
        private WinRadioButton createTemplateRadioButton;
        private WinRadioButton createConfigurationRadioButton;

    }
}
