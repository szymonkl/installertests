﻿namespace InstallerTest.Windows.AddRadios
{
    public enum AddRadios
    {
        DoNotAddRadios,
        CreateTemplate,
        CreateConfiguration
    }
}