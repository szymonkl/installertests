﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;

namespace InstallerTest.Windows.AddRadios
{
    internal interface IAddRadiosWindow
    {
        /// <summary>
        /// Dictionary which contains AddRadios options and matching radiobuttons
        /// </summary>
        IDictionary<AddRadios, WinRadioButton> AddRadiosOptions { get; }
    }
}
