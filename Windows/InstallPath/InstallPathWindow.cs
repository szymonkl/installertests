﻿using System.Collections.Generic;
using InstallerTest.Common;
using InstallerTest.Properties;
using InstallerTest.Windows.Base;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;

namespace InstallerTest.Windows.InstallPath
{
    internal class InstallPathWindow : BaseWindow, IInstallPathWindow
    {
        public WinButton InstallButton
        {
            get
            {
                if (this.installButton == null)
                {
                    this.installButton = new WinButton(this);

                    this.installButton.TechnologyName = "MSAA";
                    this.installButton.SearchProperties[UITestControl.PropertyNames.Name] = "Install";
                    this.installButton.SearchProperties[UITestControl.PropertyNames.ControlType] = "Button";
                }
                return this.installButton;
            }
        }

        public WinText ProgramFilesPath
        {
            get
            {
                if (this.programFilesPath == null)
                {
                    this.programFilesPath = new WinText(this);

                    this.programFilesPath.TechnologyName = "MSAA";
                    this.programFilesPath.SearchProperties[UITestControl.PropertyNames.Name] = Settings.Default.Path_ProgramFilesPath;
                    this.programFilesPath.SearchProperties[UITestControl.PropertyNames.ControlType] = "Text";
                }
                return this.programFilesPath;
            }
        }

        public WinText ProgramDataPath
        {
            get
            {
                if (this.programDataPath == null)
                {
                    this.programDataPath = new WinText(this);

                    this.programDataPath.TechnologyName = "MSAA";
                    this.programDataPath.SearchProperties[UITestControl.PropertyNames.Name] = Settings.Default.Path_ProgramDataPath;
                    this.programDataPath.SearchProperties[UITestControl.PropertyNames.ControlType] = "Text";
                }
                return this.programDataPath;
            }
        }

        public override IDictionary<Buttons, WinButton> Buttons => new Dictionary<Buttons, WinButton>
        {
            {Common.Buttons.Next, NextButton},
            {Common.Buttons.Cancel, CancelButton },
            {Common.Buttons.Install, InstallButton}
        };

        private WinButton installButton;
        private WinText programFilesPath;
        private WinText programDataPath;
    }
}
