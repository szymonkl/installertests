﻿using InstallerTest.Windows.Base;
using Microsoft.VisualStudio.TestTools.UITesting;
using System.Windows.Input;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;

namespace InstallerTest.Windows.SecondLicense
{
    internal class SecondLicenseWindow : BaseWindow, ISecondLicenseWindow
    {
        public WinRadioButton AcceptTermsRadiobutton
        {
            get
            {
                if (this.acceptTermsRadiobutton == null)
                {
                    this.acceptTermsRadiobutton = new WinRadioButton(this);

                    this.acceptTermsRadiobutton.TechnologyName = "MSAA";
                    this.acceptTermsRadiobutton.SearchProperties[UITestControl.PropertyNames.Name] = "I accept the terms in the license agreement";
                    this.acceptTermsRadiobutton.SearchProperties[UITestControl.PropertyNames.ControlType] = "RadioButton";
                }
                return this.acceptTermsRadiobutton;
            }
        }

        public WinButton AcceptTermsButton
        {
            get
            {
                if (this.acceptTermsButton == null)
                {
                    this.acceptTermsButton = new WinButton(this);

                    this.acceptTermsButton.TechnologyName = "MSAA";
                    this.acceptTermsButton.SearchProperties[UITestControl.PropertyNames.Name] = "I &accept the terms in the license agreement";
                    this.acceptTermsButton.SearchProperties[UITestControl.PropertyNames.ControlType] = "Window";
                }
                return this.acceptTermsButton;
            }
        }

        public WinEdit TextArea
        {
            get
            {
                if (this.textArea == null)
                {
                    this.textArea = new WinEdit(this);

                    this.textArea.TechnologyName = "MSAA";
                    this.textArea.SearchProperties[UITestControl.PropertyNames.Name] = "Please read the following license agreement carefully.";
                    this.textArea.SearchProperties[UITestControl.PropertyNames.ControlType] = "Edit";
                }
                return this.textArea;
            }
        }

        public WinRadioButton DontAcceptTermsRadiobutton
        {
            get
            {
                if (this.dontAcceptTermsRadiobutton == null)
                {
                    this.dontAcceptTermsRadiobutton = new WinRadioButton(this);

                    this.dontAcceptTermsRadiobutton.TechnologyName = "MSAA";
                    this.dontAcceptTermsRadiobutton.SearchProperties[UITestControl.PropertyNames.Name] = "I do not accept the terms in the license agreement";
                    this.dontAcceptTermsRadiobutton.SearchProperties[UITestControl.PropertyNames.ControlType] = "RadioButton";
                }
                return this.dontAcceptTermsRadiobutton;
            }
        }

        private WinRadioButton acceptTermsRadiobutton;
        private WinRadioButton dontAcceptTermsRadiobutton;
        private WinButton acceptTermsButton;
        private WinEdit textArea;

        public void AcceptLicenseTerms()
        {
            Keyboard.SendKeys(this, "A", ModifierKeys.Alt); 
        }

        public void DoNotAcceptLicenseTerms()
        {
            Keyboard.SendKeys(this, "D", ModifierKeys.Alt);
        }
    }
}
