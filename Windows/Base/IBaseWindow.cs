﻿using System.Collections.Generic;
using InstallerTest.Common;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;

namespace InstallerTest.Windows.Base
{
    internal interface IBaseWindow
    {
        /// <summary>
        /// Dictionary that contains button enums and matching button control
        /// </summary>
        IDictionary<Buttons, WinButton> Buttons { get; }
    }
}