﻿using System.Collections.Generic;
using InstallerTest.Common;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;

namespace InstallerTest.Windows.Base
{
    internal class BaseWindow : WinWindow, IBaseWindow
    {
        public WinTitleBar Title
        {
            get
            {
                if (this.title == null)
                {
                    this.title = new WinTitleBar(this);

                    this.title.TechnologyName = "MSAA";
                    this.title.SearchProperties[UITestControl.PropertyNames.ControlType] = TitleControlType;
                }
                return this.title;
            }
        }

        public WinButton NextButton
        {
            get
            {
                if (this.nextButton == null)
                {
                    this.nextButton = new WinButton(this);

                    this.nextButton.TechnologyName = "MSAA";
                    this.nextButton.SearchProperties[UITestControl.PropertyNames.Name] = "Next >";
                    this.nextButton.SearchProperties[UITestControl.PropertyNames.ControlType] = ButtonControlType;
                }
                return this.nextButton;
            }
        }

        public WinButton CancelButton
        {
            get
            {
                if (this.cancelButton == null)
                {
                    this.cancelButton = new WinButton(this);

                    this.cancelButton.TechnologyName = "MSAA";
                    this.cancelButton.SearchProperties[UITestControl.PropertyNames.Name] = "Cancel";
                    this.cancelButton.SearchProperties[UITestControl.PropertyNames.ControlType] = ButtonControlType;
                }
                return this.cancelButton;
            }
        }

        public virtual IDictionary<Buttons, WinButton> Buttons => new Dictionary<Buttons, WinButton>
        {
            {Common.Buttons.Next, NextButton},
            {Common.Buttons.Cancel, CancelButton}
        };

        private WinTitleBar title;
        private WinButton cancelButton;
        private WinButton nextButton;
        private const string WindowControlType = "Window";
        private const string ButtonControlType = "Button";
        private const string TitleControlType = "TitleBar";

        public BaseWindow()
        {
            this.SearchProperties.Add(UITestControl.PropertyNames.Name, "APX Radio Management Suite", PropertyExpressionOperator.Contains);
            this.SearchProperties.Add(UITestControl.PropertyNames.ControlType, WindowControlType);
        }
    }
}
