﻿using InstallerTest.Windows.Base;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;

namespace InstallerTest.Windows.ChooseLanguage
{
    internal class ChooseLanguageWindow : BaseWindow, IChooseLanguageWindow
    {
        public WinComboBox LanguageListComboBox
        {
            get
            {
                if (this.languageListComboBox == null)
                {
                    this.languageListComboBox = new WinComboBox(this);

                    this.languageListComboBox.TechnologyName = "MSAA";
                    this.languageListComboBox.SearchProperties[UITestControl.PropertyNames.Name] = "Select the language for this installation from the choices below.";
                    this.languageListComboBox.SearchProperties[UITestControl.PropertyNames.ControlType] = "ComboBox";
                }
                return this.languageListComboBox;
            }
        }
        
        private WinComboBox languageListComboBox;
    }
}
