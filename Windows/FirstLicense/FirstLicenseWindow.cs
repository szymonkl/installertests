﻿using InstallerTest.Windows.Base;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;

namespace InstallerTest.Windows.FirstLicense
{
    internal class FirstLicenseWindow : BaseWindow, IFirstLicenseWindow
    {
        public WinRadioButton AcceptTermsRadiobutton
        {
            get
            {
                if (this.acceptTermsRadiobutton == null)
                {
                    this.acceptTermsRadiobutton = new WinRadioButton(this);

                    this.acceptTermsRadiobutton.TechnologyName = "MSAA";
                    this.acceptTermsRadiobutton.SearchProperties[UITestControl.PropertyNames.Name] = "I accept the terms in the license agreement";
                    this.acceptTermsRadiobutton.SearchProperties[UITestControl.PropertyNames.ControlType] = "RadioButton";
                }
                return this.acceptTermsRadiobutton;
            }
        }

        public WinRadioButton DontAcceptTermsRadiobutton
        {
            get
            {
                if (this.dontAcceptTermsRadiobutton == null)
                {
                    this.dontAcceptTermsRadiobutton = new WinRadioButton(this);

                    this.dontAcceptTermsRadiobutton.TechnologyName = "MSAA";
                    this.dontAcceptTermsRadiobutton.SearchProperties[UITestControl.PropertyNames.Name] = "I do not accept the terms in the license agreement";
                    SearchProperties[UITestControl.PropertyNames.ControlType] = "RadioButton";
                }
                return this.dontAcceptTermsRadiobutton;
            }
        }

        private WinRadioButton acceptTermsRadiobutton;
        private WinRadioButton dontAcceptTermsRadiobutton;
    }
}
