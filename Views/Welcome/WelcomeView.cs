﻿using InstallerTest.Views.Base;
using InstallerTest.Views.SelectFeatures;
using InstallerTest.Windows.SelectFeatures;
using InstallerTest.Windows.Welcome;
using Microsoft.VisualStudio.TestTools.UITesting;

namespace InstallerTest.Views.Welcome
{
    internal class WelcomeView : BaseView, IWelcomeView
    {
        private readonly WelcomeWindow welcomeWindow;

        public WelcomeView(WelcomeWindow welcomeWindow) : base(welcomeWindow)
        {
            this.welcomeWindow = welcomeWindow;
        }

        public ISelectFeaturesView ClickNext()
        {
            Mouse.Click(this.welcomeWindow.NextButton);

            return new SelectFeaturesView(new SelectFeaturesWindow());
        }
    }
}
