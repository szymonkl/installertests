﻿using InstallerTest.Views.Base;
using InstallerTest.Views.SelectFeatures;

namespace InstallerTest.Views.Welcome
{
    internal interface IWelcomeView : IBaseView
    {
        /// <summary>
        /// Goes to Select Features View
        /// </summary>
        /// <returns>Select Features View</returns>
        ISelectFeaturesView ClickNext();
    }
}
