﻿using InstallerTest.Common;
using InstallerTest.Views.Base;
using InstallerTest.Views.SecondLicense;

namespace InstallerTest.Views.FirstLicense
{
    internal interface IFirstLicenseView : IBaseView
    {
        /// <summary>
        /// Clicks "Accepts license terms" radiobutton
        /// </summary>
        /// <returns>First License View</returns>
        IFirstLicenseView AcceptLicenseTerms();

        /// <summary>
        /// Clicks "Do not accept license terms" radiobutton
        /// </summary>
        /// <returns>First License View</returns>
        IFirstLicenseView DoNotAcceptTheTerms();

        /// <summary>
        /// Verifies if specifed option (accept, don't accept) is currently selected
        /// </summary>
        /// <param name="expected">License terms option to be verified</param>
        /// <returns>First License View</returns>
        IFirstLicenseView VerifyLicenseTerms(LicenseTerms expected);

        /// <summary>
        /// Goes to Second License View
        /// </summary>
        /// <returns>Second License View</returns>
        ISecondLicenseView ClickNext();
    }
}