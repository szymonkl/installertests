﻿using System;
using InstallerTest.Common;
using InstallerTest.Views.Base;
using InstallerTest.Views.SecondLicense;
using InstallerTest.Windows.FirstLicense;
using InstallerTest.Windows.SecondLicense;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InstallerTest.Views.FirstLicense
{
    internal class FirstLicenseView : BaseView, IFirstLicenseView
    {
        private readonly FirstLicenseWindow firstLicenseWindow;

        public FirstLicenseView(FirstLicenseWindow firstLicenseWindow) : base(firstLicenseWindow)
        {
            this.firstLicenseWindow = firstLicenseWindow;
        }

        public FirstLicenseView()
        {
            throw new NotImplementedException();
        }

        public IFirstLicenseView AcceptLicenseTerms()
        {
            Mouse.Click(this.firstLicenseWindow.AcceptTermsRadiobutton);

            return new FirstLicenseView(this.firstLicenseWindow);
        }

        public IFirstLicenseView DoNotAcceptTheTerms()
        {
            Mouse.Click(this.firstLicenseWindow.DontAcceptTermsRadiobutton);

            return new FirstLicenseView(this.firstLicenseWindow);
        }

        public IFirstLicenseView VerifyLicenseTerms(LicenseTerms expected)
        {
            this.firstLicenseWindow.Find();
            if (this.firstLicenseWindow.AcceptTermsRadiobutton.WaitForControlExist(30000) 
                && this.firstLicenseWindow.DontAcceptTermsRadiobutton.WaitForControlExist(30000))
            {
                switch (expected)
                {
                    case LicenseTerms.Accept:
                        Assert.IsTrue(this.firstLicenseWindow.AcceptTermsRadiobutton.Selected, "License terms are not accepted, but they should");
                        break;
                    case LicenseTerms.DoNotAccept:
                        Assert.IsTrue(this.firstLicenseWindow.DontAcceptTermsRadiobutton.Selected, "License terms are accepted but they shouldn't");
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(expected), expected, null);
                }
                
            }
            else
            {
                Assert.Fail("Cannot find Accept/Don't Accept Radio Button");
            }

            return new FirstLicenseView(this.firstLicenseWindow);
        }

        public ISecondLicenseView ClickNext()
        {
            Mouse.Click(this.firstLicenseWindow.NextButton);

            return new SecondLicenseView(new SecondLicenseWindow());
        }
    }
}
