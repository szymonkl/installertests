﻿using InstallerTest.Views.Base;
using InstallerTest.Windows.SelectFeatures;

namespace InstallerTest.Views.SelectFeatures
{
    internal interface ISelectFeaturesView : IBaseView
    {
        /// <summary>
        /// Selects specified feature
        /// </summary>
        /// <param name="feature">Feature to be selected</param>
        /// <returns>Select Features View</returns>
        ISelectFeaturesView SelectFeature(Features feature);

        /// <summary>
        /// Selects all features (including RM Config mode)
        /// </summary>
        /// <returns>Select Features View</returns>
        ISelectFeaturesView SelectAllFeatures();

        /// <summary>
        /// Verifies version of specified component
        /// </summary>
        /// <param name="feature">Component to be verified</param>
        /// <param name="expected">Expected version of the component</param>
        /// <returns>Select Features View</returns>
        ISelectFeaturesView VerifyComponentVersion(Features feature, string expected);

        /// <summary>
        /// Verifies if specified component is visible
        /// </summary>
        /// <param name="feature">Component to be verified</param>
        /// <param name="expected">Expected value: true for visible, false for not visible</param>
        /// <returns>Select Features View</returns>
        ISelectFeaturesView VerifyComponentVisible(Features feature, bool expected = true);

        /// <summary>
        /// Verifies all versions of the components except specified components
        /// </summary>
        /// <param name="componentsToSkip">Components to be excluded from version verification</param>
        /// <returns>Select Features View</returns>
        ISelectFeaturesView VerifyAllComponentsVersions(params Features[] componentsToSkip);

        /// <summary>
        /// Verifies if all components (except specified) are visible
        /// </summary>
        /// <param name="componentsToSkip">Components to be excluded from visibility verification</param>
        /// <returns>Select Features View</returns>
        ISelectFeaturesView VerifyAllComponentsVisible(params Features[] componentsToSkip);

        /// <summary>
        /// Clicks next and goes to specified view
        /// </summary>
        /// <typeparam name="TView">Expected view after clicking Next button</typeparam>
        /// <returns>Expected view</returns>
        TView ClickNext<TView>() where TView : BaseView, new();
    }
}