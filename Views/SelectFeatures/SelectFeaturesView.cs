﻿using InstallerTest.ExtensionMethods;
using InstallerTest.Windows.SelectFeatures;
using Microsoft.VisualStudio.TestTools.UITesting;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using InstallerTest.Common;
using InstallerTest.Views.Base;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InstallerTest.Views.SelectFeatures
{
    internal class SelectFeaturesView : BaseView, ISelectFeaturesView
    {
        private readonly SelectFeaturesWindow selectFeaturesWindow;

        private readonly IDictionary<Features, string> expectedVersions = new Dictionary<Features, string>
        {
            {Features.ApxCps, Properties.Settings.Default.Version_CPS},
            {Features.RadioManagementClient, Properties.Settings.Default.Version_RM},
            {Features.RadioManagementServer, Properties.Settings.Default.Version_RM},
            {Features.RadioManagementDeviceProgrammer, Properties.Settings.Default.Version_RM},
            {Features.RadioManagementJobProcessor, Properties.Settings.Default.Version_RM},
            {Features.ApxFamilyTuner, Properties.Settings.Default.Version_Tuner},
            {Features.AdvancedKeysAdministrator, Properties.Settings.Default.Version_ASK},
            {Features.ArsDataAdministrator, Properties.Settings.Default.Version_ARS},
            {Features.ApxMigrationAssistant, Properties.Settings.Default.Version_MigrationAssistant}
        };

        public SelectFeaturesView(SelectFeaturesWindow welcomeWindow) : base(welcomeWindow)
        {
            this.selectFeaturesWindow  = welcomeWindow;
        }

        public ISelectFeaturesView SelectFeature(Features feature)
        {
            this.selectFeaturesWindow.FeaturesListDictionary[feature].Checked = !this.selectFeaturesWindow.FeaturesListDictionary[feature].Checked;

            return new SelectFeaturesView(this.selectFeaturesWindow);
        }

        public ISelectFeaturesView SelectAllFeatures()
        {
            foreach (var feature in this.selectFeaturesWindow.FeaturesListDictionary)
            {
                feature.Value.Checked = true;
            }
            return new SelectFeaturesView(this.selectFeaturesWindow);
        }

        public ISelectFeaturesView VerifyComponentVersion(Features feature, string expected)
        {
            Assert.IsTrue(IsComponentVersionCorrect(feature, expected),
                $"Expected version for component {feature.ToDisplayString()} is {expected}, but actual was {ParseVersion(this.selectFeaturesWindow.ComponentsListDictionary[feature].Name)}");
            return new SelectFeaturesView(this.selectFeaturesWindow);
        }

        private bool IsComponentVersionCorrect(Features feature, string expected) => this.selectFeaturesWindow
            .ComponentsListDictionary[feature].Name.Contains(expected);

        private static string ParseVersion(string input)
        {
            return Regex.Match(input, @"(?<=Version )(.*)(?=\))").Value;
        }

        public ISelectFeaturesView VerifyAllComponentsVersions(params Features[] componentsToSkip)
        {
            var components = SelectComponentsExcept(componentsToSkip);

            var messageBuilder = new StringBuilder();
            foreach (var component in components)
            {
                if (!IsComponentVersionCorrect(component))
                {
                    messageBuilder.AppendLine(
                        $"{component.Key.ToDisplayString()} - Expected: {this.expectedVersions[component.Key]}, Actual: {ParseVersion(component.Value.Name)}");
                }
            }

            if (FoundErrors(messageBuilder))
            {
                Assert.Fail("The following versions were not correct:\n" + messageBuilder);
            }
            return new SelectFeaturesView(this.selectFeaturesWindow);
        }

        private IEnumerable<KeyValuePair<Features, WinCheckBox>> SelectComponentsExcept(
            params Features[] componentsToSkip) =>
            this.selectFeaturesWindow.ComponentsListDictionary.Where(c => !componentsToSkip.Contains(c.Key));

        private bool IsComponentVersionCorrect(KeyValuePair<Features, WinCheckBox> component) =>
            component.Value.Name.Contains(this.expectedVersions[component.Key]);

        private static bool FoundErrors(StringBuilder stringBuilder) => !string.IsNullOrEmpty(stringBuilder.ToString());

        public ISelectFeaturesView VerifyComponentVisible(Features feature, bool expected = true)
        {
            var isVisible = IsComponentVisible(feature);
            var errorMessage = isVisible
                ? $"Feature {feature.ToDisplayString()} is visible but it shouldn't"
                : $"Feature {feature.ToDisplayString()} isn't visible, but it should.";

            Assert.AreEqual(expected, isVisible, errorMessage);

            return new SelectFeaturesView(this.selectFeaturesWindow);
        }

        private bool IsComponentVisible(Features component) =>
            this.selectFeaturesWindow.ComponentsListDictionary[component].TryFind();

        public ISelectFeaturesView VerifyAllComponentsVisible(params Features[] componentsToSkip)
        {
            var messageBuilder = new StringBuilder();

            var components = SelectComponentsExcept(componentsToSkip);

            foreach (var component in components)
            {
                if (!IsComponentVisible(component))
                {
                    messageBuilder.AppendLine($"{component.Key.ToDisplayString()}");
                }
            }

            if (FoundErrors(messageBuilder))
            {
                Assert.Fail("The following components were not visible:\n" + messageBuilder);
            }

            return new SelectFeaturesView(this.selectFeaturesWindow);
        }

        private static bool IsComponentVisible(KeyValuePair<Features, WinCheckBox> component) => component.Value.TryFind();

        public TView ClickNext<TView>() where TView : BaseView, new()
        {
            Mouse.Click(this.selectFeaturesWindow.NextButton);

            return ViewFactory.CreateView<TView>();
        }
    }
}
