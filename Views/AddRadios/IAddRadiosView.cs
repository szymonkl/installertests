﻿using InstallerTest.Views.Base;
using InstallerTest.Views.FirstLicense;

namespace InstallerTest.Views.AddRadios
{
    internal interface IAddRadiosView : IBaseView
    {
        /// <summary>
        /// Verifies if specified Add Radios to RM database option is visible
        /// </summary>
        /// <param name="option">Add Radios to RM database option to be verified</param>
        /// <param name="expected">Expected value - true for visible, false for not visible</param>
        /// <returns>Add Radios view</returns>
        IAddRadiosView VerifyOptionVisible(Windows.AddRadios.AddRadios option, bool expected = true);

        /// <summary>
        /// Goes to First License View
        /// </summary>
        /// <returns>First License View</returns>
        IFirstLicenseView ClickNext();
    }
}