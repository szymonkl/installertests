﻿using InstallerTest.ExtensionMethods;
using InstallerTest.Views.Base;
using InstallerTest.Views.FirstLicense;
using InstallerTest.Windows.AddRadios;
using InstallerTest.Windows.FirstLicense;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InstallerTest.Views.AddRadios
{
    internal class AddRadiosView : BaseView, IAddRadiosView
    {
        private readonly AddRadiosWindow addRadiosWindow;

        public AddRadiosView(AddRadiosWindow addRadiosWindow)
        {
            this.addRadiosWindow = addRadiosWindow;
        }

        public AddRadiosView()
        {
            throw new System.NotImplementedException();
        }

        public IAddRadiosView VerifyOptionVisible(Windows.AddRadios.AddRadios option, bool expected = true)
        {
            var isVisible = IsOptionVisible(option);
            var errorMessage = isVisible
                ? $@"Option ""{option.ToDisplayString()}"" is visible, but it shouldn't"
                : $@"Option ""{option.ToDisplayString()}"" isn't visible but it should";

            Assert.AreEqual(expected, IsOptionVisible(option), errorMessage);
            return new AddRadiosView(this.addRadiosWindow);
        }

        private bool IsOptionVisible(Windows.AddRadios.AddRadios option) =>
             this.addRadiosWindow.AddRadiosOptions[option].TryFind();

        public IFirstLicenseView ClickNext()
        {
            Mouse.Click(this.addRadiosWindow.NextButton);

            return new FirstLicenseView(new FirstLicenseWindow());
        }
        
    }
}
