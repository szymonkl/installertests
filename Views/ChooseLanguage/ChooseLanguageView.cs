﻿using InstallerTest.Views.Welcome;
using InstallerTest.Windows.ChooseLanguage;
using InstallerTest.Windows.Welcome;
using Microsoft.VisualStudio.TestTools.UITesting;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using InstallerTest.Views.Base;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InstallerTest.Views.ChooseLanguage
{
    internal class ChooseLanguageView : BaseView, IChooseLanguageView
    {
        private readonly ChooseLanguageWindow chooseLanguageWindow;

        internal IDictionary<ChooseLanguageList, string> LanguageListDictionary => new Dictionary<ChooseLanguageList, string>
        {
            { ChooseLanguageList.Chinese, "Chinese (Traditional)" },
            { ChooseLanguageList.English, "English (United States)" },
            { ChooseLanguageList.French, "French (Canadian)" },
            { ChooseLanguageList.Portuguese, "Portuguese (Brazilian)" },
            { ChooseLanguageList.Russian, "Russian" },
            { ChooseLanguageList.Spanish, "Spanish" }
        };

        public ChooseLanguageView(ChooseLanguageWindow chooseLanguageWindow) : base(chooseLanguageWindow)
        {
            this.chooseLanguageWindow = chooseLanguageWindow;
        }

        public ChooseLanguageView()
        {
            throw new System.NotImplementedException();
        }

        public IChooseLanguageView ChooseLanguage(ChooseLanguageList chooseLanguageList)
        {
            //Expand combobox
            this.chooseLanguageWindow.LanguageListComboBox.Expanded = true;

            //Click choosen language
            foreach (var comboBoxItem in this.chooseLanguageWindow.LanguageListComboBox.Items)
            {
                if (comboBoxItem.Name.Equals(LanguageListDictionary[chooseLanguageList]))
                {
                    Mouse.Click(comboBoxItem);
                    break;
                }
            }

            return new ChooseLanguageView(this.chooseLanguageWindow);
        }

        public IWelcomeView ClickNext()
        {
            Mouse.Click(this.chooseLanguageWindow.NextButton);

            return new WelcomeView(new WelcomeWindow());
        }

        public IChooseLanguageView VerifyWindowTitleVersion(string expected)
        {
            var actual = ParseWindowTitle(this.chooseLanguageWindow.Title.DisplayText);
            Assert.AreEqual(expected, actual, $"Expected version in window title is {expected}, but actual was {actual}");

            return new ChooseLanguageView(this.chooseLanguageWindow);
        }

        private static string ParseWindowTitle(string windowTitle) =>
             windowTitle.Replace("APX Radio Management Suite ", string.Empty);
        

        public IChooseLanguageView VerifyAvailableLanguages()
        {
            var errorMessage = new StringBuilder();

            foreach (var language in LanguageListDictionary)
            {
                var foundLanguage = FindLanguageInComboBox(language);

                if (string.IsNullOrEmpty(foundLanguage))
                {
                    errorMessage.AppendLine(language.Value);
                }
            }

            if (ErrorsOccourred(errorMessage))
            {
                Assert.Fail("The following languages were not found:\n" + errorMessage);
            }

            return new ChooseLanguageView(this.chooseLanguageWindow);
        }

        private string FindLanguageInComboBox(KeyValuePair<ChooseLanguageList, string> language) =>
            this.chooseLanguageWindow.LanguageListComboBox.GetContent()
                .SingleOrDefault(l => l.Equals(language.Value));

        private static bool ErrorsOccourred(StringBuilder errorMessage) =>
             !string.IsNullOrEmpty(errorMessage.ToString());

    }
}
