﻿using InstallerTest.Views.Base;
using InstallerTest.Views.Welcome;

namespace InstallerTest.Views.ChooseLanguage
{
    internal interface IChooseLanguageView : IBaseView
    {
        /// <summary>
        /// Selects specified language option
        /// </summary>
        /// <param name="chooseLanguageList">Language option to be selected</param>
        /// <returns>Choose language view</returns>
        IChooseLanguageView ChooseLanguage(ChooseLanguageList chooseLanguageList);
       
        /// <summary>
        /// Goes to Welcome View
        /// </summary>
        /// <returns>Welcome View</returns>
        IWelcomeView ClickNext();
        
        /// <summary>
        /// Verifies version included in installer window title
        /// </summary>
        /// <param name="expected">Expected suite version</param>
        /// <returns>Choose Language View</returns>
        IChooseLanguageView VerifyWindowTitleVersion(string expected);

        /// <summary>
        /// Verifies if all language options are visible in combobox
        /// </summary>
        /// <returns>Choose Language View</returns>
        IChooseLanguageView VerifyAvailableLanguages();
    }
}