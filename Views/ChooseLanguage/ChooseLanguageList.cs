﻿namespace InstallerTest.Views.ChooseLanguage
{
    internal enum ChooseLanguageList
    {
        Chinese,
        English,
        French,
        Portuguese,
        Russian,
        Spanish
    }
}
