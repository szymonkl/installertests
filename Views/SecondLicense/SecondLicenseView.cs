﻿using System;
using InstallerTest.Common;
using InstallerTest.Views.Base;
using InstallerTest.Views.Readme;
using InstallerTest.Windows.Readme;
using InstallerTest.Windows.SecondLicense;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InstallerTest.Views.SecondLicense
{
    internal class SecondLicenseView : BaseView, ISecondLicenseView
    {
        private readonly SecondLicenseWindow secondLicenseWindow;

        public SecondLicenseView(SecondLicenseWindow secondLicenseWindow) : base(secondLicenseWindow)
        {
            this.secondLicenseWindow = secondLicenseWindow;
        }

        public SecondLicenseView()
        {
            throw new NotImplementedException();
        }

        public ISecondLicenseView AcceptLicenseTerms()
        {
            this.secondLicenseWindow.AcceptLicenseTerms();
            
            return new SecondLicenseView(this.secondLicenseWindow);
        }

        public ISecondLicenseView DoNotAcceptTheTerms()
        {
            this.secondLicenseWindow.DoNotAcceptLicenseTerms();

            return new SecondLicenseView(this.secondLicenseWindow);
        }

        public ISecondLicenseView VerifyLicenseTerms(LicenseTerms expected)
        {
            this.secondLicenseWindow.Find();
            if (this.secondLicenseWindow.AcceptTermsRadiobutton.WaitForControlExist(10000) && this.secondLicenseWindow.DontAcceptTermsRadiobutton.WaitForControlExist(10000))
            {
                switch (expected)
                {
                    case LicenseTerms.Accept:
                        Assert.IsTrue(this.secondLicenseWindow.AcceptTermsRadiobutton.Selected, "License terms are not accepted, but they should");
                        break;
                    case LicenseTerms.DoNotAccept:
                        Assert.IsTrue(this.secondLicenseWindow.DontAcceptTermsRadiobutton.Selected, "License terms are accepted but they shouldn't");
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(expected), expected, null);
                } 
            }
            else
            {
                Assert.Fail("Cannot find Accept/Don't Accept Radio Button");
            }

            return new SecondLicenseView(this.secondLicenseWindow);
        }

        public IReadmeView ClickNext()
        {
            Mouse.Click(this.secondLicenseWindow.NextButton);

            return new ReadmeView(new ReadmeWindow());
        }
    }
}
