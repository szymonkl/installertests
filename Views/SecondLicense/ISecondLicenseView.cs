﻿using InstallerTest.Common;
using InstallerTest.Views.Base;
using InstallerTest.Views.Readme;

namespace InstallerTest.Views.SecondLicense
{
    internal interface ISecondLicenseView : IBaseView
    {
        /// <summary>
        /// Clicks "Accepts license terms" radiobutton
        /// </summary>
        /// <returns>Second License View</returns>
        ISecondLicenseView AcceptLicenseTerms();

        /// <summary>
        /// Clicks "Do not accept license terms" radiobutton
        /// </summary>
        /// <returns>Second License View</returns>
        ISecondLicenseView DoNotAcceptTheTerms();

        /// <summary>
        /// Verifies if specifed option (accept, don't accept) is currently selected
        /// </summary>
        /// <param name="expected">License terms option to be verified</param>
        /// <returns>Second License View</returns>
        ISecondLicenseView VerifyLicenseTerms(LicenseTerms expected);

        /// <summary>
        /// Goes to Readme View
        /// </summary>
        /// <returns>Readme View</returns>
        IReadmeView ClickNext();
    }
}