﻿using InstallerTest.Views.Base;
using InstallerTest.Views.Install;

namespace InstallerTest.Views.InstallPath
{
    internal interface IInstallPathView : IBaseView
    {
        /// <summary>
        /// Clicks Install button and starts installation
        /// </summary>
        /// <returns>Install View</returns>
        InstallView ClickInstall();

        /// <summary>
        /// Compares Program Files path with path specified in app.config file
        /// </summary>
        /// <returns>Install Path View</returns>
        IInstallPathView VerifyDefaultProgramFilesPath();

        /// <summary>
        /// Compares Program Data path with path specified in app.config file
        /// </summary>
        /// <returns>Install Path View</returns>
        IInstallPathView VerifyDefaultProgramDataPath();
    }
}
