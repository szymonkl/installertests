﻿using InstallerTest.Views.Base;
using InstallerTest.Views.Install;
using InstallerTest.Windows.Install;
using InstallerTest.Windows.InstallPath;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InstallerTest.Views.InstallPath
{
    internal class InstallPathView : BaseView, IInstallPathView
    {
        private readonly InstallPathWindow installPathWindow;

        public InstallPathView(InstallPathWindow installPathWindow) : base(installPathWindow)
        {
            this.installPathWindow = installPathWindow;
        }

        public InstallPathView()
        {
            throw new System.NotImplementedException();
        }

        public InstallView ClickInstall()
        {
            Mouse.Click(this.installPathWindow.InstallButton);
            return new InstallView(new InstallWindow());
        }

        public IInstallPathView VerifyDefaultProgramFilesPath()
        {
            var isVisible = this.installPathWindow.ProgramFilesPath.TryFind();
            var expectedPath =
                this.installPathWindow.ProgramFilesPath.SearchProperties[UITestControl.PropertyNames.Name]; 

            Assert.IsTrue(isVisible,
                $"Default Program Files path is different than {expectedPath} or it is not visible");

            return new InstallPathView(this.installPathWindow);
        }

        public IInstallPathView VerifyDefaultProgramDataPath()
        {
            var isVisible = this.installPathWindow.ProgramDataPath.TryFind();
            var expectedPath =
                this.installPathWindow.ProgramDataPath.SearchProperties[UITestControl.PropertyNames.Name];

            Assert.IsTrue(isVisible,
                $"Default Program Data path is different than {expectedPath} or it is not visible");

            return new InstallPathView(this.installPathWindow);
        }
    }
}
