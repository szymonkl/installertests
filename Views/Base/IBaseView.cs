﻿using InstallerTest.Common;

namespace InstallerTest.Views.Base
{
    internal interface IBaseView
    {
        /// <summary>
        /// Verifies if specified button is enabled
        /// </summary>
        /// <typeparam name="TView">View that is expected to be returned</typeparam>
        /// <param name="button">Button to be verified</param>
        /// <param name="expected">Expected value - true for enabled, false for disabled</param>
        /// <returns>Expected view</returns>
        TView VerifyButtonEnabled<TView>(Buttons button, bool expected = true) where TView : BaseView, new();

        /// <summary>
        /// Verifies if specified button is visible
        /// </summary>
        /// <typeparam name="TView">View that is expected to be returned</typeparam>
        /// <param name="button">Button to be verified</param>
        /// <param name="expected">Expected value - true for visible, false for invisible</param>
        /// <returns>Expected view</returns>
        TView VerifyButtonVisible<TView>(Buttons button, bool expected = true) where TView : BaseView, new();
    }
}
