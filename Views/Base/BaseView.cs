﻿using InstallerTest.Common;
using InstallerTest.ExtensionMethods;
using InstallerTest.Windows.Base;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InstallerTest.Views.Base
{
    internal class BaseView : IBaseView
    {
        private readonly BaseWindow window;

        public BaseView(BaseWindow window)
        {
            this.window = window;
        }

        protected BaseView() { }

        public TView VerifyButtonEnabled<TView>(Buttons button, bool expected = true) where TView : BaseView, new()
        {
            var isEnabled = this.window.Buttons[button].Enabled;
            var errorMessage = isEnabled
                ? $"Button {button.ToDisplayString()} is enabled but it shouldn't"
                : $"Button {button.ToDisplayString()} isn't enabled but it should";

            Assert.AreEqual(expected, isEnabled, errorMessage);

            return ViewFactory.CreateView<TView>();
        }

        public TView VerifyButtonVisible<TView>(Buttons button, bool expected = true) where TView : BaseView, new()
        {
            var isVisible = this.window.Buttons[button].TryFind();

            var errorMessage = isVisible
                ? $"Button {button.ToDisplayString()} is visible but it shouldn't"
                : $"Button {button.ToDisplayString()} isn't visible but it should";

            Assert.AreEqual(expected, isVisible, errorMessage);

            return ViewFactory.CreateView<TView>();
        }
    }
}
