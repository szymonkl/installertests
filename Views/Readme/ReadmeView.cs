﻿using System.Text.RegularExpressions;
using InstallerTest.Views.Base;
using InstallerTest.Windows.Readme;
using Microsoft.VisualStudio.TestTools.UITesting;
using InstallerTest.Views.InstallPath;
using InstallerTest.Windows.InstallPath;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InstallerTest.Views.Readme
{
    internal class ReadmeView : BaseView, IReadmeView
    {
        private readonly ReadmeWindow readmeWindow;

        public ReadmeView(ReadmeWindow readmeWindow) : base(readmeWindow)
        {
            this.readmeWindow = readmeWindow;
        }

        public IReadmeView VerifyReadmeVersion(string expected)
        {
            var text = this.readmeWindow.ReadmeText.Text;
            Assert.IsTrue(text.Contains(expected), $"Readme text doesn't contain expected version - {expected}");
            Assert.AreEqual(2, new Regex(expected).Matches(text).Count, $"Readme text doesn't contain two occourrences of expected version - {expected}");

            return new ReadmeView(this.readmeWindow);
        }

        public InstallPathView ClickNext()
        {
            Mouse.Click(this.readmeWindow.NextButton);
            return new InstallPathView(new InstallPathWindow());
        }
    }
}
