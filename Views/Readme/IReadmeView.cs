﻿using InstallerTest.Views.Base;
using InstallerTest.Views.InstallPath;

namespace InstallerTest.Views.Readme
{
    internal interface IReadmeView : IBaseView
    {
        /// <summary>
        /// Verifies if versions mentioned in Readme file
        /// </summary>
        /// <param name="expected">Expected version number</param>
        /// <returns>Readme view</returns>
        IReadmeView VerifyReadmeVersion(string expected);

        /// <summary>
        /// Goes to Install Path View
        /// </summary>
        /// <returns>Install View Path</returns>
        InstallPathView ClickNext();
    }
}