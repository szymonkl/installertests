﻿using InstallerTest.Views.Base;
using InstallerTest.Windows.Install;

namespace InstallerTest.Views.Install
{
    internal class InstallView : BaseView, IInstallView
    {
        private InstallWindow installWindow;

        public InstallView(InstallWindow installWindow) : base(installWindow)
        {
            this.installWindow = installWindow;
        }

        public InstallView()
        {
            throw new System.NotImplementedException();
        }
    }
}
