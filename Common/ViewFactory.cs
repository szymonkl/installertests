﻿using System;

namespace InstallerTest.Common
{
    internal class ViewFactory
    {
        public static T CreateView<T>() where T : new()
        {
            var view = default(T);

            //finding window which matches to view by convention
            var fullWindowName = typeof(T).FullName?.Replace("Views", "Windows").Replace("View", "Window");

            if (fullWindowName != null)
            {
                var window = Activator.CreateInstance(Type.GetType(fullWindowName) ??
                                                      throw new InvalidOperationException(
                                                          $"Cannot find window with name {fullWindowName}"));
                view = (T)Activator.CreateInstance(typeof(T), window);
            }

            return view;
        }
    }
}
