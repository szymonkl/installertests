﻿using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InstallerTest.Engine
{
    [CodedUITest]
    public class InstallerTestBase : IInstallerTestBase
    {
        [TestInitialize]
        public void TestInitialize()
        {
            InstallerInitializer.InitializePlayback();
            InstallerInitializer.Start();
        }

        [TestCleanup]
        public void TestCleanup()
        {
            InstallerInitializer.Stop();
        }

        public InstallerInitializer InstallerInitializer => new InstallerInitializer();
    }
}
