﻿namespace InstallerTest.Engine
{
    public interface IInstallerTestBase
    {
        /// <summary>
        /// Method that is run before every test
        /// </summary>
        void TestInitialize();

        /// <summary>
        /// Method that is run after every test 
        /// </summary>
        void TestCleanup();
        
        /// <summary>
        /// Test initializer object
        /// </summary>
        InstallerInitializer InstallerInitializer { get; }
    }
}