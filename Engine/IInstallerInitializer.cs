﻿namespace InstallerTest.Engine
{
    public interface IInstallerInitializer
    {
        /// <summary>
        /// Starts new instance of the installer
        /// </summary>
        void Start();

        /// <summary>
        /// Closes the current instance of the installer
        /// </summary>
        void Stop();

        /// <summary>
        /// Determines whether any instance of the installer is already running
        /// </summary>
        /// <returns>Returns true if the installer is running, otherwise returns false</returns>
        bool IsRunning();

        /// <summary>
        /// Waits for closing the installer
        /// </summary>
        void WaitForExit();

        /// <summary>
        /// Returns the path to the installer
        /// </summary>
        string GetInstallerPath { get; }

        /// <summary>
        /// Initializes global playback settings
        /// </summary>
        void InitializePlayback();
    }
}