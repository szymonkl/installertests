﻿using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Microsoft.VisualStudio.TestTools.UITesting;
using System;
using System.Diagnostics;

namespace InstallerTest.Engine
{
    public class InstallerInitializer : IInstallerInitializer
    {
        private ApplicationUnderTest applicationUnderTest;

        public void Start()
        {
            //Close any existing installation instances
            KillProcess("Setup", "Setup Suite Launcher Unicode", "Motorola Solutions, Inc.");

            //Start new installation instance
            this.applicationUnderTest = ApplicationUnderTest.Launch(GetInstallerPath);
        }

        public void Stop()
        {
            if(this.applicationUnderTest != null && IsRunning())
            {
                this.applicationUnderTest.Close();
                this.applicationUnderTest = null;
            }
        }

        public bool IsRunning()
        {
            if (this.applicationUnderTest != null && this.applicationUnderTest.Process != null)
            {
                return !this.applicationUnderTest.Process.HasExited;
            }
            return false;
        }

        public void WaitForExit()
        {
            this.applicationUnderTest?.Process.WaitForExit();
        }

        public void InitializePlayback()
        {
            if (!Playback.IsInitialized)
                Playback.Initialize();

            Playback.PlaybackSettings.WaitForReadyLevel = WaitForReadyLevel.Disabled;
            Playback.PlaybackSettings.MaximumRetryCount = 10;
            Playback.PlaybackSettings.ShouldSearchFailFast = false;
            Playback.PlaybackSettings.DelayBetweenActions = 600;

#if DEBUG
            Playback.PlaybackSettings.SearchTimeout = 1200;
#else
            Playback.PlaybackSettings.SearchTimeout = 3000;
#endif
        }

        private static void KillProcess(string processName, string fileDescription, string companyName)
        {
            foreach (var process in Process.GetProcessesByName(processName))
            {
                if (!process.HasExited && process.MainModule.FileVersionInfo.FileDescription.Equals(fileDescription) && process.MainModule.FileVersionInfo.CompanyName.Equals(companyName))
                {
                    process.Kill();
                }
            }
        }

        /// <returns>Null if there is no such environment variable</returns>
        public string GetInstallerPath => Environment.GetEnvironmentVariable("ASTRO_SUITE_INSTALLER_PATH", EnvironmentVariableTarget.User);
    }
}
