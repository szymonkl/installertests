﻿using InstallerTest.Common;
using InstallerTest.Engine;
using InstallerTest.Properties;
using InstallerTest.Views.AddRadios;
using InstallerTest.Views.ChooseLanguage;
using InstallerTest.Views.FirstLicense;
using InstallerTest.Views.InstallPath;
using InstallerTest.Views.SecondLicense;
using InstallerTest.Windows.AddRadios;
using InstallerTest.Windows.SelectFeatures;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InstallerTest.Tests
{
    [CodedUITest]
    public class InstallationTest : InstallerTestBase
    {
        [TestMethod]
        [TestCategory("RMCEnabled")]
        public void CheckComponentsVersions()
        {
            ViewFactory.CreateView<ChooseLanguageView>()
                .ChooseLanguage(ChooseLanguageList.English)
                .ClickNext()
                .ClickNext()
                .VerifyAllComponentsVersions();
        }
        
        [TestMethod]
        [TestCategory("RMCDisabled")]
        public void CheckComponentsVersionsExceptRmc()
        {
            ViewFactory.CreateView<ChooseLanguageView>()
                .ChooseLanguage(ChooseLanguageList.English)
                .ClickNext()
                .ClickNext()
                .VerifyAllComponentsVersions(Features.RadioManagementClient);
        }

        [TestMethod]
        [TestCategory("RMCEnabled")]
        public void CheckComponentsVisibility()
        {
            ViewFactory.CreateView<ChooseLanguageView>()
                .ChooseLanguage(ChooseLanguageList.English)
                .ClickNext()
                .ClickNext()
                .VerifyAllComponentsVisible();
        }

        [TestMethod]
        [TestCategory("RMCDisabled")]
        public void CheckComponentsVisibilityExceptRmc()
        {
            ViewFactory.CreateView<ChooseLanguageView>()
                .ChooseLanguage(ChooseLanguageList.English)
                .ClickNext()
                .ClickNext()
                .VerifyAllComponentsVisible(Features.RadioManagementClient);
        }

        [TestMethod]
        [TestCategory("RMCIndependent")]
        public void CheckSuiteVersion()
        {
            ViewFactory.CreateView<ChooseLanguageView>()
                .VerifyWindowTitleVersion(Settings.Default.Version_RM);
        }

        [TestMethod]
        [TestCategory("RMCEnabled")]
        public void CheckRmcVisibilityWhenEnabled()
        {
            ViewFactory.CreateView<ChooseLanguageView>()
                .ChooseLanguage(ChooseLanguageList.English)
                .ClickNext()
                .ClickNext()
                .VerifyComponentVisible(Features.RadioManagementClient)
                .SelectFeature(Features.RadioManagementServer)
                .ClickNext<AddRadiosView>()
                .VerifyOptionVisible(AddRadios.DoNotAddRadios)
                .VerifyOptionVisible(AddRadios.CreateTemplate)
                .VerifyOptionVisible(AddRadios.CreateConfiguration);
        }

        [TestMethod]
        [TestCategory("RMCDisabled")]
        public void CheckRmcVisibilityWhenDisabled()
        {
            ViewFactory.CreateView<ChooseLanguageView>()
                .ChooseLanguage(ChooseLanguageList.English)
                .ClickNext()
                .ClickNext()
                .VerifyComponentVisible(Features.RadioManagementClient, false)
                .SelectFeature(Features.RadioManagementServer)
                .ClickNext<AddRadiosView>()
                .VerifyOptionVisible(AddRadios.DoNotAddRadios)
                .VerifyOptionVisible(AddRadios.CreateTemplate);
        }

        [TestMethod]
        [TestCategory("RMCIndependent")]
        public void CheckCpsInstallationSteps()
        {
            ViewFactory.CreateView<ChooseLanguageView>()
                .ChooseLanguage(ChooseLanguageList.English)
                .ClickNext()
                .ClickNext()
                .SelectFeature(Features.Arabic)
                .SelectFeature(Features.Chinese)
                .SelectFeature(Features.French)
                .SelectFeature(Features.Hebrew)
                .SelectFeature(Features.Portuguese)
                .SelectFeature(Features.Russian)
                .SelectFeature(Features.Spanish)
                .ClickNext<FirstLicenseView>()
                .AcceptLicenseTerms()
                .ClickNext()
                .VerifyLicenseTerms(LicenseTerms.DoNotAccept)
                .AcceptLicenseTerms()
                .ClickNext()
                .ClickNext()
                .VerifyButtonVisible<InstallPathView>(Buttons.Install)
                .VerifyButtonEnabled<InstallPathView>(Buttons.Install);
        }

        [TestMethod]
        [TestCategory("RMCIndependent")]
        public void CheckRmInstallationSteps()
        {
            ViewFactory.CreateView<ChooseLanguageView>()
                .ChooseLanguage(ChooseLanguageList.English)
                .ClickNext()
                .ClickNext()
                .SelectFeature(Features.RadioManagementServer)
                .ClickNext<AddRadiosView>()
                .ClickNext()
                .AcceptLicenseTerms()
                .ClickNext()
                .AcceptLicenseTerms()
                .ClickNext()
                .ClickNext()
                .VerifyButtonVisible<InstallPathView>(Buttons.Install)
                .VerifyButtonEnabled<InstallPathView>(Buttons.Install);
        }

        [TestMethod]
        [TestCategory("RMCIndependent")]
        public void CheckAvailableLanguages()
        {
            ViewFactory.CreateView<ChooseLanguageView>()
                .VerifyAvailableLanguages();
        }

        [TestMethod]
        [TestCategory("RMCIndependent")]
        public void CheckReadmeVersion()
        {
            ViewFactory.CreateView<ChooseLanguageView>()
                .ChooseLanguage(ChooseLanguageList.English)
                .ClickNext()
                .ClickNext()
                .ClickNext<FirstLicenseView>()
                .AcceptLicenseTerms()
                .ClickNext()
                .AcceptLicenseTerms()
                .ClickNext()
                .VerifyReadmeVersion(Settings.Default.Version_Readme);
        }

        [TestMethod]
        [TestCategory("RMCIndependent")]
        public void CheckLicensesAcceptance()
        {
            ViewFactory.CreateView<ChooseLanguageView>()
                .ChooseLanguage(ChooseLanguageList.English)
                .ClickNext()
                .ClickNext()
                .ClickNext<FirstLicenseView>()
                .VerifyButtonEnabled<FirstLicenseView>(Buttons.Next, false)
                .VerifyLicenseTerms(LicenseTerms.DoNotAccept)
                .AcceptLicenseTerms()
                .VerifyButtonEnabled<FirstLicenseView>(Buttons.Next)
                .ClickNext()
                .VerifyButtonEnabled<FirstLicenseView>(Buttons.Next, false)
                .VerifyLicenseTerms(LicenseTerms.DoNotAccept)
                .AcceptLicenseTerms()
                .VerifyButtonEnabled<SecondLicenseView>(Buttons.Next)
                .ClickNext();
        }

        [TestMethod]
        [TestCategory("RMCIndependent")]
        public void CheckDefaultInstallationPaths()
        {
            ViewFactory.CreateView<ChooseLanguageView>()
                .ChooseLanguage(ChooseLanguageList.English)
                .ClickNext()
                .ClickNext()
                .SelectFeature(Features.RadioManagementServer)
                .ClickNext<AddRadiosView>()
                .ClickNext()
                .AcceptLicenseTerms()
                .ClickNext()
                .AcceptLicenseTerms()
                .ClickNext()
                .ClickNext()
                .VerifyDefaultProgramFilesPath()
                .VerifyDefaultProgramDataPath();
        }
    }
}
