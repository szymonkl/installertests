﻿using System;
using InstallerTest.Common;
using InstallerTest.Windows.AddRadios;
using InstallerTest.Windows.SelectFeatures;

namespace InstallerTest.ExtensionMethods
{
    public static class EnumsExtensions
    {
        public static string ToDisplayString(this Features features)
        {
            switch (features)
            {
                case Features.ApxCps:
                    return "APX CPS";
                case Features.French:
                    return "Language - French";
                case Features.Spanish:
                    return "Language - Spanish";
                case Features.Portuguese:
                    return "Language - Portuguese";
                case Features.Hebrew:
                    return "Language - Hebrew";
                case Features.Russian:
                    return "Language - Russian";
                case Features.Chinese:
                    return "Language - Chinese";
                case Features.Arabic:
                    return "Language - Arabic";
                case Features.RadioManagementClient:
                    return "RM Configuration Mode";
                case Features.RadioManagementServer:
                    return "RM Server";
                case Features.AutoUpdateEnable:
                    return "Auto Update Enable";
                case Features.RadioManagementDeviceProgrammer:
                    return "RM Device Programmer";
                case Features.RadioManagementJobProcessor:
                    return "RM Job Processor";
                case Features.ApxFamilyTuner:
                    return "APX Family Tuner";
                case Features.AdvancedKeysAdministrator:
                    return "Advanced Keys Administrator";
                case Features.ArsDataAdministrator:
                    return "ARS Data Administrator";
                case Features.ApxMigrationAssistant:
                    return "APX Migration Assistant";
                default:
                    throw new ArgumentOutOfRangeException(nameof(features), features, null);
            }
        }

        public static string ToDisplayString(this AddRadios addRadios)
        {
            switch (addRadios)
            {
                case AddRadios.DoNotAddRadios:
                    return "Do not automatically add new radio to the RM database";
                case AddRadios.CreateTemplate:
                    return "Automatically read new radios and create a template";
                case AddRadios.CreateConfiguration:
                    return "Automatically read new radios and create a configuration";
                default:
                    throw new ArgumentOutOfRangeException(nameof(addRadios), addRadios, null);
            }
        }

        public static string ToDisplayString(this Buttons button)
        {
            switch (button)
            {
                case Buttons.Next:
                    return "Next";
                case Buttons.Cancel:
                    return "Cancel";
                case Buttons.Install:
                    return "Install";
                default:
                    throw new ArgumentOutOfRangeException(nameof(button), button, null);
            }
        }
    }
}
